# Génération de labyrinthe

Les tests d'un projet visent 3 objectifs :

* La documentation fonctionnelle du projet
* La non régression
* Faire émerger le code

Nous allons nous concentrer sur l'émergence du code

# Définition

Un labyrinthe parfait ne comporte pas de boucle interne, c'est-à-dire que chaque cellule est reliée à toutes les autres et, ce, de manière unique.

Voici un exemple de labyrinthe parfait, les murs sont représentés par des `#` :

```
################
      #     #  #
####  #  #  #  #
#  #     #     #
#  #######  ####
#           #  #
####  ####  #  #
#     #        #
#  #  ####  #  #
#  #  #     #
################
```

```
################
      #     #  #
####  ####  #  #
#              #
####  ####  ####
#     #     #  #
#  #  #  ####  #
#  #  #        #
#######  #######
```

Notre objectif est de générer des labyrinthes parfait de façon totalement aléatoire.

Vous aurez remarqué que les cellules font 1 caractère de haut et 2 de large.

# Rappel des étapes du TDD

Développer ce générateur de labyrinthe en TDD.

* *Test* :
    * Lancer le chronomètre
    * Écrire un test
    * Vérifier qu'il est rouge (Tout compile sans erreur mais le test ne passe pas)
    * Commiter avec en message `Test : intention fonctionnel et intention de code`
    * Arrêter le chronomètre
* *Implémentation* :
    * Lancer le chronomètre
    * Faire passer le test au vert, et bien évidement, conserver tous les précédents au verts aussi.
    * Commiter avec en message `Impl : intention fonctionnel`
    * Arrêter le chronomètre
* *Restructuration* :
    * Lancer le chronomètre
    * Restructurer le code
    * Et bien évidement, conserver tous les tests verts
    * Commiter avec en message `Refacto : description des éléments changés`
    * Arrêter le chronomètre

# Étape 0 : Initialisation sans entrée ni sortie

L'initialisation du labyrinthe se fait en donnant le nombre de cellules vides, 

Labyrinthe de 1 sur 1 :

```
####
#  #
####
```

Labyrinthe de 2 sur 2 :

```
#######
#  #  #
#######
#  #  #
#######
```

Exemple de test : 

```java
@Test
void should_initialize_one_cell_maze() {
    String expectedRender1x1 = """
            ####
            #  #
            ####""";
    assertEquals(expectedRender1x1, Maze.of(1,1).render());
}

@Test
void should_initialize_maze_with_size_of_empty_cell() {
    String expectedRender2x2 = """
            #######
            #  #  #
            #######
            #  #  #
            #######""";
    assertEquals(expectedRender2x2, Maze.of(2,2).render());
}
```

# Étape 1 : une entrée et une sortie

```
#######
   #  #
#######
#  #  #
#  ####
```

Écrire les tests et faire l'implémentation pour valider que le générateur de labyrinthes est capable de produire une entrée sur 1 bord et une sortie sur un autre. Vous aurez probablement besoin d'un Générateur de position fixe dans les tests et d'un générateur de position d'entrée et de sortie aléatoire pour le code final.

# Étape 2 : 1 chemin

Comme chaque cellule est reliée à toutes les autres de manière unique, on peut partir d'une grille avec tous les murs :

```
#######
#  #  #
#######
#  #  #
#######
```
Dans notre exemple, nous avons 4 chemins d'une cellule.

Puis ouvrir 1 mur aléatoirement pour construire 1 chemin plus grand :

```
#######
#     #
#######
#  #  #
#######
```
Dans cet exemple, nous avons 1 chemin de 2 cellules et 2 chemins d'une cellule.

Puis recommencer à ouvrir 1 mur aléatoirement entre 2 chemins :

```
#######
#     #
#######
#     #
#######
```
Dans ce nouvel exemple, nous avons 2 chemins de 2 cellules.

Puis recommencer :

```
#######
#     #
####  #
#     #
#######
```

Et s'arrêter lorsqu'il ne reste qu'un chemin.

# Étape 3 : Paramétrable

Faire en sorte de pouvoir générer des labyrinthe de taille plus ou moins grande, avec 1 entrée et N sortie.

# Étape 4 : Résolution du labyrinthe

Faire en TDD un algorythme de résolution du labyrinthe en main droite et afficher le résultat du parcour :

```
################
····· #     #  #
####· ####  #  #
#··········    #
####· ####· ####
#···· #···· #  #
#· #· #· ####  #
#· #· #·       #
#######· #######
```

# Étape 5 : Graphique

Passer sur un affichage en JavaFX
